
## Introduction:
This is an assignment project which consists of task app. This app provides an Espresso test for testing the app. In this app you can add multiple task, check mark and remove them.

## Test Setup:
For this project, I created Espresso test with the use of Barista Library. Barista removes most of the boilerplate and verbosity of common Espresso tasks. It make test more readable.

## Run Espresso test
    1: Open the desired app module folder and navigate to the test 
    2: If you are using the Android view navigate to java > com/example/abn_tasklist/TaskTests.java or if you are using the Project view inside the window, navigate to src > androidTest > java > com/example/abn_tasklist/TaskTests.java.
    3:Right-click on the test and click Run ‘TaskTests’


## Pending Tasks:
    - Save data to SharedPreferences so that the data is no removed after that the app is closed
    - Implement junit5 to make use of @DisplayName
    - Integration with CI i.e. Jenkins, Travis or Circle CI


## Bugs:
    - Deleted tasks are not completely deleted from the array list
    - empty_list_placeholder is sometimes visible even when there are tasks in the list


