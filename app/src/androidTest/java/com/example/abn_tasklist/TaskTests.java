package com.example.abn_tasklist;

import static com.adevinta.android.barista.assertion.BaristaCheckedAssertions.assertChecked;
import static com.adevinta.android.barista.assertion.BaristaCheckedAssertions.assertUnchecked;
import static com.adevinta.android.barista.assertion.BaristaErrorAssertions.assertErrorDisplayed;
import static com.adevinta.android.barista.assertion.BaristaHintAssertions.assertHint;
import static com.adevinta.android.barista.assertion.BaristaListAssertions.assertDisplayedAtPosition;
import static com.adevinta.android.barista.assertion.BaristaRecyclerViewAssertions.assertRecyclerViewItemCount;
import static com.adevinta.android.barista.assertion.BaristaVisibilityAssertions.assertDisplayed;
import static com.adevinta.android.barista.assertion.BaristaVisibilityAssertions.assertNotDisplayed;
import static com.adevinta.android.barista.interaction.BaristaClickInteractions.clickOn;
import static com.adevinta.android.barista.interaction.BaristaClickInteractions.longClickOn;
import static com.adevinta.android.barista.interaction.BaristaEditTextInteractions.clearText;
import static com.adevinta.android.barista.interaction.BaristaListInteractions.scrollListToPosition;
import static com.example.abn_tasklist.MainActivity.tasksList;
import static com.example.abn_tasklist.util.Actions.clickCheckBoxAtPosition;

import android.util.Log;

import com.adevinta.android.barista.rule.BaristaRule;
import com.example.abn_tasklist.screens.TaskListScreen;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import java.util.ArrayList;
import java.util.List;

public class TaskTests {

    private static final String HOMEWORK = "Complete my homework";

    public TaskListScreen taskListScreen;

    @Rule
    public BaristaRule<MainActivity> activityRule = BaristaRule.create(MainActivity.class);

    public @Rule
    TestName name = new TestName();

    @Before
    public void setUp() {
        taskListScreen = new TaskListScreen();
        Log.i("Info", "[START] - Launch Test: " + name.getMethodName());
    }

    @After
    public void tearDown() {
        Log.i("Info", "[FINISH] - Test: " + name.getMethodName());
    }

    private void startActivity() {
        activityRule.launchActivity();
    }

    /**
     * Verify add new task popup
     */
    @Test
    public void verifyAddTaskPopUp() {
        startActivity();

        taskListScreen.clickPlusButton();
        assertDisplayed("Title");
        assertHint(R.id.title_input, R.string.edit_text_placeholder);
        assertDisplayed(R.id.task_add);
    }

    /**
     * Verify task order
     */
    @Test
    public void verifyAddTask() {
        // create dummy data tasks
        tasksList.addAll(createSampleTasks(15));

        startActivity();

        taskListScreen.addNewTask(HOMEWORK);
        scrollListToPosition(R.id.tasks_recyclerview, 0);
        assertDisplayed("Task 1");

        scrollListToPosition(R.id.tasks_recyclerview, 15);
        assertDisplayed(HOMEWORK);
    }

    /**
     * Verify error message at the text input field
     */
    @Test
    public void verifyPopUpErrorMessage() {
        startActivity();

        taskListScreen.clickPlusButton();
        taskListScreen.typeNewTask("i");
        taskListScreen.clickOnAddTaskButton();
        assertErrorDisplayed(R.id.title_input, "Oeps.. mininum text length should be 2");

        clearText(R.id.title_input);
        taskListScreen.typeNewTask("This message has 30 characters");
        taskListScreen.clickOnAddTaskButton();
        assertErrorDisplayed(R.id.title_input, "Oeps.. maximum text length should be 30");
    }

    /**
     * Verify checkbox of a single task
     */
    @Test
    public void verifyCheckbox() {
        startActivity();
        //Add a new task
        taskListScreen.addNewTask(HOMEWORK);

        assertDisplayed(HOMEWORK);
        assertUnchecked(R.id.task_status);

        clickOn(R.id.task_status);
        assertChecked(R.id.task_status);
    }

    /**
     * Verify snackbar message and if checked tasks are deleted via delete all button
     */
    @Test
    public void verifyMultipleCheckboxes() {
        // create dummy data tasks
        tasksList.addAll(createSampleTasks(3));

        startActivity();

        clickCheckBoxAtPosition(0);
        assertDisplayed("Task: Task 1 done");

        clickCheckBoxAtPosition(1);
        assertDisplayed("Task: Task 2 done");

        clickCheckBoxAtPosition(2);
        assertDisplayed("Task: Task 3 done");

        taskListScreen.deleteAllTasks();
        assertDisplayed(R.id.empty_list_placeholder);
    }

    /**
     * Verify if unchecked task are deleted via delete all button
     */
    @Test
    public void verifyDeleteAllTaskViaOptionMenu() {
        // create dummy data tasks
        tasksList.addAll(createSampleTasks(10));

        startActivity();
        assertNotDisplayed(R.id.empty_list_placeholder);

        taskListScreen.deleteAllTasks();
        assertDisplayed(R.id.empty_list_placeholder);
    }

    /**
     * Verify task one by one and check if correct task is removed
     */
    @Test
    public void verifyDeleteOneByOne() {
        // create dummy data tasks
        tasksList.addAll(createSampleTasks(5));

        startActivity();

        assertRecyclerViewItemCount(R.id.tasks_recyclerview, 5);

        longClickOn("Task 1");
        // Ensure that right task is removed
        assertDisplayedAtPosition(R.id.tasks_recyclerview, 0, "Task 2");
        assertDisplayedAtPosition(R.id.tasks_recyclerview, 1, "Task 3");
        assertDisplayedAtPosition(R.id.tasks_recyclerview, 2, "Task 4");
        assertDisplayedAtPosition(R.id.tasks_recyclerview, 3, "Task 5");

        longClickOn("Task 3");
        assertDisplayedAtPosition(R.id.tasks_recyclerview, 0, "Task 2");
        assertDisplayedAtPosition(R.id.tasks_recyclerview, 1, "Task 4");
        assertDisplayedAtPosition(R.id.tasks_recyclerview, 2, "Task 5");

        longClickOn("Task 4");
        assertDisplayedAtPosition(R.id.tasks_recyclerview, 0, "Task 2");
        assertDisplayedAtPosition(R.id.tasks_recyclerview, 1, "Task 5");

        longClickOn("Task 5");
        assertDisplayedAtPosition(R.id.tasks_recyclerview, 0, "Task 2");

        longClickOn("Task 2");
        assertDisplayed(R.id.empty_list_placeholder);
    }

    private List<TaskModel> createSampleTasks(int amount) {
        List<TaskModel> modelList = new ArrayList<>();
        for (int i = 1; i <= amount; i++) {
            modelList.add(new TaskModel("Task " + i));
        }
        return modelList;
    }

}