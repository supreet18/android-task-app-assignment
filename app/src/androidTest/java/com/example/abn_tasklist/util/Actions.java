package com.example.abn_tasklist.util;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

import android.view.View;

import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.contrib.RecyclerViewActions;

import com.example.abn_tasklist.R;

import org.hamcrest.Matcher;

/**
 * This class contains custom actions that can be used for automation testing
 */
public class Actions {

    public static ViewAction clickChildViewWithId(final int id) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on a child view with specified id.";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View v = view.findViewById(id);
                v.performClick();
            }
        };
    }

    public static void clickCheckBoxAtPosition(int position) {
        onView(withId(R.id.tasks_recyclerview)).perform(
                RecyclerViewActions.actionOnItemAtPosition(
                        position, clickChildViewWithId(R.id.task_status)));
    }
}