package com.example.abn_tasklist.screens;

import static com.adevinta.android.barista.interaction.BaristaClickInteractions.clickOn;
import static com.adevinta.android.barista.interaction.BaristaEditTextInteractions.typeTo;
import static com.adevinta.android.barista.interaction.BaristaMenuClickInteractions.openMenu;

import com.example.abn_tasklist.R;

public class TaskListScreen {

    public void addNewTask(String task) {
        clickPlusButton();
        typeNewTask(task);
        clickOnAddTaskButton();
    }

    public void clickPlusButton() {
        clickOn(R.id.fab);
    }

    public void typeNewTask(String task) {
        typeTo(R.id.title_input, task);
    }

    public void clickOnAddTaskButton() {
        clickOn(R.id.task_add);
    }

    public void deleteAllTasks() {
        openMenu();
        clickOn(R.string.delete_all);
    }
}