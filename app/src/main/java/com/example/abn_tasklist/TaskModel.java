package com.example.abn_tasklist;

public class TaskModel {

    private final String task;
    private boolean done;

    public TaskModel(String name) {
        this.task = name;
        this.done = false;
    }

    public String getTask() {
        return task;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
