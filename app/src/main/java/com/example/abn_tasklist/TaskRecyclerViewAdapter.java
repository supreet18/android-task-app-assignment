package com.example.abn_tasklist;

import static com.example.abn_tasklist.MainActivity.checkIfEmpty;
import static com.example.abn_tasklist.MainActivity.tasksList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

public class TaskRecyclerViewAdapter extends RecyclerView.Adapter<TaskRecyclerViewAdapter.ViewHolder> {

    private final Context context;

    public TaskRecyclerViewAdapter(Context ctx) {
        this.context = ctx;
    }

    @Override
    public TaskRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_layout, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    /*
     * the recycler view will create a view for each task when we scroll through the list.
     * once a task is out of the screen, the graphical elements will be destroyed.
     * that is why I have to initialize every time the graphical elements with actual values.
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TaskModel selectedTask = tasksList.get(position);

        // update the data from the model (task) to the view (ui elements)
        holder.taskName.setText(selectedTask.getTask());

        //when item from the display has been touched, we update the information of the model.
        holder.taskState.setOnCheckedChangeListener(null); //reset the listener when the item is recreated when scrolling
        holder.taskState.setChecked(selectedTask.isDone()); //update the display with the status of the task
        holder.taskState.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                //display a short message when we click the checkbox
                Snackbar snackbar = Snackbar.make(buttonView, "Task: " + selectedTask.getTask() + " done", Snackbar.LENGTH_SHORT);
                snackbar.show();
                //save the status of the task
                selectedTask.setDone(true);
            } else {
                selectedTask.setDone(false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tasksList.size();
    }

    /**
     * this class gets a reference to all the graphical elements of a task.
     * this is called an inner-class because it is defined inside another class.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView taskName;
        public CheckBox taskState;


        public ViewHolder(View view) {
            super(view);
            taskName = view.findViewById(R.id.task_title);
            taskState = view.findViewById(R.id.task_status);

            //item click event listener for long press
            view.setOnLongClickListener(v -> {
                tasksList.remove(getAdapterPosition());
                notifyItemRemoved(getAdapterPosition());

                notifyItemRangeChanged(getAdapterPosition(), tasksList.size());

                Snackbar snackbar = Snackbar.make(v, "Task is removed", Snackbar.LENGTH_SHORT);
                snackbar.show();

                checkIfEmpty();
                return true;
            });
        }
    }
}
