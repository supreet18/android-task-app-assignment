package com.example.abn_tasklist;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static List<TaskModel> tasksList = new ArrayList<>();
    @SuppressLint("StaticFieldLeak")
    public static TextView placeholder;
    private static RecyclerView tasksRecyclerView;
    boolean isAllFieldsChecked = false;
    private RecyclerView.Adapter mAdapter;
    private String task;
    private TextView taskTitle;

    public MainActivity() {
    }

    //check if task list is empty so it can show the placeholder text or not
    public static void checkIfEmpty() {
        if (tasksRecyclerView.getAdapter().getItemCount() == 0) {
            placeholder.setVisibility(View.VISIBLE);
        } else {
            placeholder.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        placeholder = findViewById(R.id.empty_list_placeholder);
        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(view -> showTaskPopup());

        //tasks recycler view
        tasksRecyclerView = findViewById(R.id.tasks_recyclerview);

        // in content do not change the layout size of the RecyclerView
        tasksRecyclerView.setHasFixedSize(false);

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        tasksRecyclerView.setLayoutManager(mLayoutManager);

        DividerItemDecoration itemDecor = new DividerItemDecoration(tasksRecyclerView.getContext(), DividerItemDecoration.HORIZONTAL);
        tasksRecyclerView.addItemDecoration(itemDecor);

        // specify an adapter
        mAdapter = new TaskRecyclerViewAdapter(this);
        tasksRecyclerView.setAdapter(mAdapter);

        checkIfEmpty();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @SuppressLint("NotifyDataSetChanged")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        tasksList.clear();
        mAdapter.notifyDataSetChanged();
        Snackbar snackbar = Snackbar.make(this.findViewById(android.R.id.content), "All tasks are deleted", Snackbar.LENGTH_SHORT);
        snackbar.show();
        checkIfEmpty();
        return true;
    }

    @SuppressLint("NotifyDataSetChanged")
    private void showTaskPopup() {
        try {
            //We need to get the instance of the LayoutInflater, use the context of this activity
            LayoutInflater inflater = (LayoutInflater) MainActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            //Inflate the view from a predefined XML layout (no need for root id, using entire layout)
            View layout = inflater.inflate(R.layout.task_popup, null);

            //create a focusable PopupWindow with the given layout and correct size
            final PopupWindow pw = new PopupWindow(layout, ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT, true);
            pw.setBackgroundDrawable(new ColorDrawable(Color.GRAY));

            taskTitle = layout.findViewById(R.id.title_input);

            //Button to close the pop-up
            layout.findViewById(R.id.task_add).setOnClickListener(v -> {
                //extract the filled in information
                task = taskTitle.getText().toString();
                isAllFieldsChecked = checkAllFields();

                if (isAllFieldsChecked) {
                    //create a new task
                    TaskModel newTask = new TaskModel(task);
                    //  //add the task to the list
                    tasksList.add(newTask);

                    placeholder.setVisibility(View.GONE);
                    Snackbar snackbar = Snackbar.make(v, "Task: " + task + " is added", Snackbar.LENGTH_SHORT);
                    snackbar.show();

                    //notify that the data has changed in the list
                    mAdapter.notifyDataSetChanged();
                    tasksRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
                    //close the window
                    pw.dismiss();
                }
            });
            //display the pop-up in the center
            pw.showAtLocation(layout, Gravity.CENTER, 0, 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkAllFields() {
        //check if the amount of characters in should not be lower then 2
        if (task.length() <= 2) {
            taskTitle.setError(getResources().getString(R.string.minimal_text_error_message));
            return false;
        }

        //check if the amount of characters in should not be longer then 30
        if (task.length() >= 30) {
            taskTitle.setError(getResources().getString(R.string.maximum_text_error_message));
            return false;
        }
        // after all validation return true.
        return true;
    }
}